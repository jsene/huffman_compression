package huffman;
import java.io.*;
import java.util.*;
/**
 *
 * @author jacobsenecal
 */
public class Driver {
    //Holds the original text of the file
    public static ArrayList<Character> file = new ArrayList<>();
    
    public static void main(String[] args) {
          //Load initial text file and determine character frequencies
          //Path is the location of the desired text file
          String path = "/Users/jacobsenecal/Documents/Montana_State/CS MS/Data Struct and Algorthms/Huffman/originalText.txt";
          HashMap<Character,Integer> text = loadInfo(path);
          
          //Create Huffman Tree
          Huffman tree = new Huffman(text);
          Node root = tree.buildHuffmanTree();
          
          //Create key for each character
          int[] codes = new int[tree.getHeight(root)];
          int j = 0;
          HashMap<Character,String> encoding = tree.createCodes(root,codes,j);
          
          //Print keys
          System.out.println("Character Codes");
          encoding.entrySet().forEach((entry) -> {
              System.out.println(entry.getKey() + ": " + entry.getValue());
          });
          
          //Write compressed file, boolean true flags write() to write the encoded text
          try {
          write(file,encoding,"encodedText.txt",true);
          } catch (IOException e) {
              System.out.println(e.getMessage());
          }
          
          //Decode compressed file and print text
          ArrayList<Character> originalFile = decode(root,"encodedText.txt");
          System.out.println("\nOriginal Message");
          originalFile.forEach((letter) -> {
              System.out.print(letter);
        });
          
          //Write decoded text to file, boolean false flags write() to write unencoded text
          try {
          write(originalFile,encoding,"decodedText.txt",false);
          } catch (IOException e){
              System.out.println(e.getMessage());
          }
    }
    
    //Method to read in a text file
    public static HashMap<Character,Integer> loadInfo(String fname) {
        HashMap<Character,Integer> characterMap = new HashMap();
        String in = "";
        try {
            Scanner sc = new Scanner(new File(fname));
            while (sc.hasNext()) {
                in = sc.nextLine();
                for (int j = 0; j < in.length(); j++) {
                    file.add(in.charAt(j));
                    if(!characterMap.containsKey(in.charAt(j))) {
                        characterMap.put(in.charAt(j),1);
                    } else {
                        characterMap.put(in.charAt(j), characterMap.get(in.charAt(j)) + 1);
                    }
                }
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + e);
        }
        return characterMap;
    }
    
    //Write encoded text to file
    public static void write(ArrayList<Character> text, HashMap<Character, String> encoding, String filename, boolean compress) throws IOException {
        String dir = System.getProperty("user.dir");
        dir += "/" + filename;
        try {
            BufferedWriter outputWriter;
            outputWriter = new BufferedWriter(new FileWriter(dir));
            if (compress) {
                for (Character curChar : text) {
                    outputWriter.write(encoding.get(curChar));
                }
            } else {
                for (Character curChar : text) {
                    outputWriter.write(curChar);
                }
            }
            outputWriter.flush();
            outputWriter.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    
    //Decode the compressed text file
    public static ArrayList<Character> decode(Node root,String filename) {
        String dir = System.getProperty("user.dir");
        dir += "/" + filename;
        String in = "";

        Node curNode = root;
        ArrayList<Character> decodedMessage = new ArrayList<>();
        try {
            Scanner sc = new Scanner(new File(dir));
            while (sc.hasNext()) {
                in = sc.nextLine();
                for (int j = 0; j < in.length(); j++) {
                    if (curNode.getLeft() == null && curNode.getRight() == null) {
                        decodedMessage.add(curNode.getData());
                        curNode = root;
                        j--;
                    } else if (in.charAt(j) == '0') {
                        curNode = curNode.getLeft();
                    } else {
                        curNode = curNode.getRight();
                    }
                }
                decodedMessage.add(curNode.getData());
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.err.println("File not found: " + e);
        }
        return decodedMessage;
    }
}
