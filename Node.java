package huffman;

/**
 *
 * @author jacobsenecal
 */
public class Node implements Comparable {
    private Node left;
    private Node right;
    private Node parent;
    private double frequency;
    private char data;
    
    public Node(double frequency, char data) {
        this.frequency = frequency;
        this.data = data;
        this.left = null;
        this.right = null;
    }
    
    public int compareTo(Object inNode) {
        //return 1 if object is greater than, -1 if object is less than, 0 if equivalent
        Node secondNode = (Node) inNode;
        
        if (secondNode.frequency > frequency) {
            return 1;
        } else if (secondNode.frequency < frequency) {
            return -1;
        } else {
            return 0;
        }
    }
    
    public void setRight(Node right) {
        this.right = right;
    }
    
    public void setLeft(Node left) {
        this.left = left;
    }
    
    public void setParent(Node parent) {
        this.parent = parent;
    }
    
    public void setData(char data) {
        this.data = data;
    }
    
    public char getData() {
        return data;
    }
    
    public double getFrequency() {
        return this.frequency;
    }
    
    public Node getLeft() {
        return left;
    }
    
    public Node getRight() {
        return right;
    }
    
    public Node getParent() {
        return parent;
    }
}
