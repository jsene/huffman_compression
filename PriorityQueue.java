package huffman;
import java.util.ArrayList;
/**
 *
 * @author jacobsenecal
 * Priority Queue implemented as Minimum Heap, stored as ArrayList;
 */
public class PriorityQueue {
    private ArrayList<Node> heap;
   
    public PriorityQueue() {
         heap = new ArrayList<>();
    }

    private int parent(int j) { return (j-1)/2; }
    private int left(int j) { return 2*j + 1; }
    private int right(int j) { return 2*j + 2; }
    private boolean hasLeft(int j) { return left(j) < heap.size(); }
    private boolean hasRight(int j) { return right(j) < heap.size(); }
    
    private void swap(int i, int j) {
        Node temp = heap.get(i);
        heap.set(i,heap.get(j));
        heap.set(j,temp);
    }
    
    private void upheap(int j) {
        while (j > 0) {
            int p = parent(j);
            if (heap.get(p).compareTo(heap.get(j)) >= 0) break; 
            swap(j,p);
            j=p;
        }
    }
    
    private void downheap(int j) {
        while (hasLeft(j)) {
            int leftIndex = left(j);
            int smallChildIndex = leftIndex;
            if (hasRight(j)) {
                int rightIndex = right(j);
                if (heap.get(rightIndex).compareTo(heap.get(leftIndex)) > 0) {
                    smallChildIndex = rightIndex;
                }
            }
            if (heap.get(j).compareTo(heap.get(smallChildIndex)) >= 0) break;
            swap(j,smallChildIndex);
            j = smallChildIndex;
        }
    }
    
    public int size() { return heap.size(); }
    
    public Node min() {
        if (heap.isEmpty()) {
            return null;
        } else {
            return heap.get(0);
        }
    }
    
    public Node insert(Node newest) {
        heap.add(newest);
        upheap(heap.size()-1);
        return newest;
    }
    
    public Node removeMin() {
        if (heap.isEmpty()) {
            return null;
        } else {
            Node min = heap.get(0);
            swap(0,heap.size()-1);
            heap.remove(heap.size()-1);
            downheap(0);
            return min;
        }
    }
    
    public void printContents() {
        for (Node next:heap) {
            System.out.println(next.getData() + "  " + next.getFrequency());
        }
    }
  
}
