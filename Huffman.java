package huffman;

import java.util.*;

/**
 *
 * @author jacobsenecal
 */
public class Huffman {
    private HashMap<Character,Integer> charFreq;
    private HashMap<Character,String> codes;
    
    public Huffman(HashMap<Character,Integer> charFreq) {
        this.charFreq = charFreq;
        this.codes = new HashMap();
    }
    
    public Node buildHuffmanTree() {
        PriorityQueue minHeap = new PriorityQueue();
        for (Map.Entry<Character, Integer> entry : charFreq.entrySet()) {
            Node newNode = new Node(entry.getValue(),entry.getKey());
            minHeap.insert(newNode);
        }
        
        while (minHeap.size() > 1) {
            Node e1 = minHeap.removeMin();
            Node e2 = minHeap.removeMin();
            
            Node newNode = new Node(e1.getFrequency()+e2.getFrequency(),'+');
            newNode.setLeft(e1);
            newNode.setRight(e2);
            
            minHeap.insert(newNode);
        }
        Node root = minHeap.removeMin();
        return root;
    }
    
    public HashMap<Character,String> createCodes(Node root, int[] code, int cur) {
        if (root.getLeft() != null) {
            code[cur] = 0;
            createCodes(root.getLeft(),code,cur+1);
        }
        
        if (root.getRight() != null) {
            code[cur] = 1;
            createCodes(root.getRight(),code,cur+1);
        }
        
        if (root.getRight() == null && root.getLeft() == null) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < cur; i++) {
                sb.append(code[i]);
            }

            codes.put(root.getData(),sb.toString());
        } 
        return codes;
    }
    
    public int getHeight(Node root) {
        if (root == null) {
            return 0;
        } else {
            return(1 + Math.max(getHeight(root.getLeft()),getHeight(root.getRight())));
        }
    }
}
